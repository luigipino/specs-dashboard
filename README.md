# README #

This is the openStack Horion dashboard plugin for SPECS

### What is this repository for? ###

* The plugin adds the SPECS panel to the SPECS project in horizon interface; It interacts with SPECS applications in order to manage local resources.
* 0.1

## How do I get set up? ##
In order to install the pugin in an existing Openstack instance:
 
 * Create a db in the local instance of mysql in openstack controller (it is automatically replicated in HA environments)
 * Download source code in /opt/SPECS_dashboard on each controller machine
 * Create and Install the plugin distribution on your controller (see section create plugin)
 * In case of multiple controller install plugin on each controller.
 * enable plugin on each controller
 * restart httpd on each controller

## Configuration ##

SPECS Configuration are available in the Options panel in the horizon dashboard

## Dependencies ##

This plugin was developed under devstack  0.0.1.dev8244
It was tested under Openstack Kilo, installed with Mirantis Fuel Community Edition 6.1 with CentOS hypervisors 

### DB Creation ####
In  mysql console

    CREATE database SPECS;
    use SPECS;
    CREATE table options (component varchar(32) not null, ipv4 int unsigned, primary key component));
    grant all on SPECS.* to 'specs' identified by '<password>'
    INSERT into options values ("ChefWorkstation",inet_aton("<ip>"));
    INSERT into options values ("Implementation",inet_aton("<ip>"));
    INSERT into options values ("ServiceManager",inet_aton("<ip>"));
    INSERT into options values ("SLAManager",inet_aton("<ip>"));
    INSERT into options values ("SLOManager",inet_aton("<ip>"));
    INSERT into options values ("UserManager",inet_aton("<ip>"));


Note that a dump of dump is available in downloads.

### Create and Install Plugin ###
To create a plugin distribution on your machine use the following commands:

    git clone https://bitbucket.org/cerict/specs-dashboard.git
    cd specs-dashboard
    python setup.py sdist
    cd dist
    pip install SPECS-dashboard-<version>.tar.gz

### Enable Plugin in a controller ###

    cd /opt/specs-dashboard/SPECS_dashboard/enabled
    cp _*SPECS.py /usr/share/openstack_dashboard/openstack_dashboard/local/enabled 

Note: the last path is the one of the horizon installation, customize in case it is different

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact