#!/bin/bash

echo "Installing SPECS Dashboard plugin on this controller"

echo "Update from git"
git pull

echo "create dist"
rm dist/SPECS*tar.gz
python setup.py sdist

cd dist

echo "Uninstall old version"
pip uninstall SPECS-dashboard

echo "Install new version"
pip install SPECS*tar.gz

echo "Wait before restarting"
sleep 5

echo "restart httpd"
service httpd restart
