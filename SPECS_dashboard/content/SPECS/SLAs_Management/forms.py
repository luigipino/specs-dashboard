# Copyright 2015 SPECS Project - CeRICT

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#    http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# @author  Umberto Villano villano@unisannio.it
# @author  Antonio Punzo antonio.punzo@studenti.unisannio.it
# @author  Antonio Macolino antonio.macolino@gmail.com
# @author  Luigi Pino pinoluigi@hotmail.com

from django.utils.translation import ugettext_lazy as _

from horizon import forms
from horizon import exceptions

import api

class GetAnnotations(forms.SelfHandlingForm):
    id = forms.CharField(label=_('ID'), widget=forms.TextInput(attrs={'readonly': 'readonly'}))
    annotations = forms.CharField(label=_('Annotations'),
                                  widget=forms.Textarea(attrs={'rows': 35, 'readonly': 'readonly'}))

    def handle(self, request, data):
        pass


class AnnotateSLA(forms.SelfHandlingForm):
    id = forms.CharField(label=_('ID'), widget=forms.TextInput(attrs={'readonly': 'readonly'}))
    ann_id = forms.CharField(label=_('annotation ID'))
    description = forms.CharField(label=_('Description'), widget=forms.Textarea(attrs={'rows': 25}))

    def handle(self, request, data):
        try:
            return api.annotate_sla(data['id'], data['ann_id'], data['description'])
        except Exception:
            exceptions.handle(request,
                              _('Unable to annotate sla.'))

class ViewImplementationPlan(forms.SelfHandlingForm):
    id = forms.CharField(label=_('ID'), widget=forms.TextInput(attrs={'readonly': 'readonly'}))
    impl_plan = forms.CharField(label=_('Implementation Plan'),
                                  widget=forms.Textarea(attrs={'rows': 35, 'readonly': 'readonly'}))

    def handle(self, request, data):
        pass            

