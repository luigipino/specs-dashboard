# Copyright 2015 SPECS Project - CeRICT

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#    http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# @author  Umberto Villano villano@unisannio.it
# @author  Antonio Punzo antonio.punzo@studenti.unisannio.it
# @author  Antonio Macolino antonio.macolino@gmail.com
# @author  Luigi Pino pinoluigi@hotmail.com

from collections import defaultdict

from django.utils.translation import ugettext_lazy as _

from horizon import tables
from horizon import exceptions
from horizon import messages

import api

#class for filter action based on ID, status and customer
#class SearchSLA(tables.FilterAction):
#    name = "search"
#    verbose_name = _('Search SLA')
#    icon = 'search'
#    filter_type = "server"
#    filter_choices = (
#    ('id', _("ID =")),
#    ('status', _("Status =")),
#    ('customer', _("Customer ="))
#    )
#    needs_preloading = True
#
#    def filter(self, table, SLAs, filter_string):
#	filter_field = table.get_filter_field()
#	if (filter_field == 'id' and filter_string):
#            lower_filter_string = filter_string.lower()
#            return [SLA for SLA in SLAs if lower_filter_string in SLA.id.lower()]
#    if (filter_field == 'status' and filter_string):
#            lower_filter_string = filter_string.lower()
#            return [SLA for SLA in SLAs if lower_filter_string in SLA.status.lower()]
#	if (filter_field == 'customer' and filter_string):
#            lower_filter_string = filter_string.lower()
#            return [SLA for SLA in SLAs if lower_filter_string in SLA.customer.lower()]
#	return SLAs


class FixedFilterSLA(tables.FixedFilterAction):
    def get_fixed_buttons(self):
        buttons = []
        buttons.append(dict(text=_('Alerted SLAs'), value='ALERTED', icon='exclamation-triangle'))
        buttons.append(dict(text=_('Violated SLAs'), value='VIOLATED', icon='exclamation'))
        buttons.append(dict(text=_('Other SLAs'), value='OTHER', icon='globe'))
        return buttons

    def categorize(self, table, SLAs):
        filtered = defaultdict(list)
        for sla in SLAs:
            filtered[sla.hidden_status].append(sla)
        return filtered


class GetAnnotationsSLA(tables.LinkAction):
    name = 'annotations'
    verbose_name = _('Get Annotations')
    icon = 'file-text-o'
    url = 'horizon:SPECS:SLAs_Management:annotations'
    classes = ('ajax-modal',)


class AnnotateTableData(tables.LinkAction):
    name = 'annotate'
    verbose_name = _('Annotate SLA')
    icon = 'sticky-note-o'
    url = 'horizon:SPECS:SLAs_Management:annotate'
    classes = ('ajax-modal',)


class ViewImplementationPlan(tables.LinkAction):
    name = 'viewPlan'            
    verbose_name = _("View Implementation Plan")        
    icon = 'file-text-o'
    url = 'horizon:SPECS:SLAs_Management:impl_plan'
    classes = ('ajax-modal',)
    
    def allowed(self, request, datum):
        return (datum.status.lower() != 'pending' and datum.status.lower() != 'negotiating' and datum.status.lower() != 'terminating')


class OwnerSignSLA(tables.Action):
    name = 'sign'
    verbose_name = _('Sign SLA')
    requires_input = False
    icon = 'pencil'
    classes = ('ajax-update',)

    def allowed(self, request, datum):
        return datum.status.lower() == 'negotiating'

    def single(self, data_table, request, object_id):
        try:
            res = api.sign_sla(str(object_id))
            messages.success(request, _(' SLA ' + str(object_id) + ' signed successfully. '))
            return res
        except Exception:
            exceptions.handle(request, _(' Unable to sign SLA. '))


class TerminateSLA(tables.Action):
    name = 'terminate'
    verbose_name = _("Terminate")
    requires_input = False
    icon = 'power-off'
    classes = ('ajax-update',)

    def allowed(self, request, datum):
        return datum.status.lower() == 'observed' or datum.status.lower() == 'proactive-redressing' \
               or datum.status.lower() == 'remediating' or datum.status.lower() == 'negotiating' \
               or datum.status.lower() == 'renegotiating'

    def single(self, data_table, request, object_id):
        try:
            return api.terminate_sla(object_id)
        except Exception:
            exceptions.handle(request,
                              _(' Unable to terminate SLA. '))
   

class UpdateRow(tables.Row):
    ajax = True

    def get_data(self, request, id):
        return api.SLA(id)

    def load_cells(self, sla=None):
        super(UpdateRow, self).load_cells(sla)
        sla = self.datum
        status = sla.hidden_status
        self.classes.append('category-' + status)



class SLAsTable(tables.DataTable):
    id = tables.Column('id', verbose_name=_('ID'))
    status = tables.Column('status', verbose_name=_('Status'))
    customer = tables.Column('customer', verbose_name=_('Customer'))
    hidden_status = tables.Column('hidden_status', verbose_name=_('Hidden Status'), hidden= True)
    multi_select = False

    class Meta:
        name = 'SLAs'
        verbose_name = _('SLAs')
        status_columns = ['status']
        multi_select = False
        row_class = UpdateRow
        table_actions = (FixedFilterSLA,)
        row_actions = (OwnerSignSLA, ViewImplementationPlan, AnnotateTableData, GetAnnotationsSLA, TerminateSLA,)
