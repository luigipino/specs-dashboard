# Copyright 2015 SPECS Project - CeRICT

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#    http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# @author  Umberto Villano villano@unisannio.it
# @author  Antonio Punzo antonio.punzo@studenti.unisannio.it
# @author  Antonio Macolino antonio.macolino@gmail.com
# @author  Luigi Pino pinoluigi@hotmail.com

from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse_lazy
from django.core.urlresolvers import reverse

from horizon import tables
from horizon import forms
from horizon import exceptions

import forms as project_forms
from tables import SLAsTable
import api


class IndexView(tables.DataTableView):
    template_name = 'SPECS/SLAs_Management/index.html'
    page_title = _("SLAs Management")
    table_class = SLAsTable

    def get_data(self):
        return api.get_slas()

class AnnotateSLAView(forms.ModalFormView):
    form_class = project_forms.AnnotateSLA
    modal_id = 'annotate_sla_modal'
    modal_header = _('Annotate SLA')
    template_name = 'SPECS/SLAs_Management/annotate.html'
    success_url = reverse_lazy('horizon:SPECS:SLAs_Management:index')
    page_title = _('Annotate SLA')
    submit_label = _('Annotate')
    submit_url = 'horizon:SPECS:SLAs_Management:annotate'

    def get_initial(self):
        return {'id': self.kwargs['id']}

    def get_context_data(self, **kwargs):
        context = super(AnnotateSLAView, self).get_context_data(**kwargs)
        id = self.kwargs['id']
        context['id'] = id
        context['submit_url'] = reverse(self.submit_url, args=[id])
        return context


class GetAnnotationsView(forms.ModalFormView):
    form_class = project_forms.GetAnnotations
    modal_id = 'get_annotations_modal'
    modal_header = _('Get Annotations')
    template_name = 'SPECS/SLAs_Management/annotations.html'
    success_url = reverse_lazy('horizon:SPECS:SLAs_Management:index')
    page_title = _('Get Annotations')
    submit_label = _('Add Annotation')
    submit_url = 'horizon:SPECS:SLAs_Management:annotations'


    def _get_annotations(self):
        try:
            return api.get_annotations(self.kwargs['id'])
        except Exception:
            exceptions.handle(self.request,
                              _('Unable to retrieve annotations.'))


    def get_initial(self):
        return {'id': self.kwargs['id'], 'annotations': self._get_annotations()}

    def get_context_data(self, **kwargs):
        context = super(GetAnnotationsView, self).get_context_data(**kwargs)
        id = self.kwargs['id']
        context['sla_id'] = id
        context['annotations'] = self._get_annotations()
        return context


class ViewImplementationPlanView(forms.ModalFormView):
    form_class = project_forms.ViewImplementationPlan
    modal_id = 'view_implementation_plan_modal'
    modal_header = _('View Implementation Plan')
    template_name = 'SPECS/SLAs_Management/impl_plan.html'
    success_url = reverse_lazy('horizon:SPECS:SLAs_Management:index')
    page_title = _('View Implementation Plan')
    submit_label = _('Ok')
    submit_url = 'horizon:SPECS:SLAs_Management:impl_plan'


    def _get_impl_plan(self):
        try:
            return api.get_implementation_plan(self.kwargs['id'])
        except Exception:
            exceptions.handle(self.request,
                              _('Unable to retrieve Implementation Plan.'))


    def get_initial(self):
        return {'id': self.kwargs['id'], 'impl_plan': self._get_impl_plan()}

    def get_context_data(self, **kwargs):
        context = super(ViewImplementationPlanView, self).get_context_data(**kwargs)
        id = self.kwargs['id']
        context['sla_id'] = id
        context['impl_plan'] = self._get_impl_plan()
        return context
