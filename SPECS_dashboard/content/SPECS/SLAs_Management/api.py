# Copyright 2015 SPECS Project - CeRICT

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#    http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# @author  Umberto Villano villano@unisannio.it
# @author  Antonio Punzo antonio.punzo@studenti.unisannio.it
# @author  Antonio Macolino antonio.macolino@gmail.com
# @author  Luigi Pino pinoluigi@hotmail.com

import os
import requests
import json
from lxml import etree
from datetime import datetime
import time
import ConfigParser

from SPECS_dashboard.content.SPECS.SPECS_Options import api as options_api


SLA_Manager_URL = 'http://$$:8080/sla-manager/cloud-sla/slas/'
Implementation_Plan_URL = 'http://$$:8080/implementation-api/impl-plans/'
RESOURCES_DIR = 'resources'
CONFIG_FILE = 'specs_options.properties'

def get_file_Component_IP(component):
    basepath = os.path.dirname(__file__)
    filepath = os.path.abspath(os.path.join(basepath, '..', RESOURCES_DIR, CONFIG_FILE))
    config = ConfigParser.RawConfigParser()
    config.read(filepath)
    IP = config.get('SPECSComponentsSection', component)   
    return IP

def get_Component_IP(component):
    option = options_api.get_option(component)
    return option.ip_address

def get_slas():
    IP = get_Component_IP('SLAManager')
    url = SLA_Manager_URL.replace('$$', IP)
    r = requests.get(url)
    root = etree.fromstring(r.content)
    SLAs = []
    for child in root:
        SLAs.append(SLA(child.attrib['id']))

    return SLAs


def get_sla_status(id):
    IP = get_Component_IP('SLAManager')
    url = SLA_Manager_URL.replace('$$', IP)
    r = requests.get(url + str(id) + '/status')
    if r.status_code != 200:
        raise Exception
    else:
        return r.content


def get_sla_customer(id):
    IP = get_Component_IP('SLAManager')
    url = SLA_Manager_URL.replace('$$', IP)
    r = requests.get(url + str(id) + '/customer')
    if r.status_code != 200:
        raise Exception
    else:
        return r.content


def sign_sla(id):
    IP = get_Component_IP('SLAManager')
    url = SLA_Manager_URL.replace('$$', IP)
    ret = url + str(id) + '/sign'
    r = requests.post(url + str(id) + '/sign')
    if r.status_code != 204:
        raise Exception
    else:
        return r.content


def terminate_sla(id):
    IP = get_Component_IP('SLAManager')
    url = SLA_Manager_URL.replace('$$', IP)
    r = requests.post(url + str(id) + '/terminate')
    if r.status_code != 204:
        raise Exception
    else:
        return r.content



def get_annotations(id):
    IP = get_Component_IP('SLAManager')
    url = SLA_Manager_URL.replace('$$', IP)
    r = requests.get(url + str(id) + '/annotations')
    if r.status_code != 200:
        raise Exception
    else:
        root = etree.fromstring(r.content)
        return etree.tostring(root, pretty_print=True)


def annotate_sla(id, ann_id, description):
    IP = get_Component_IP('SLAManager')
    url = SLA_Manager_URL.replace('$$', IP)
    timestamp = datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    data = {'id': ann_id, 'description': description, 'timestamp': timestamp}
    r = requests.post(url + str(id) + '/annotations', headers=headers, data=data)
    if r.status_code != 204:
        raise Exception
    else:
        return True
    
def get_implementation_plan(id):
    IP = get_Component_IP('Implementation')
    url = Implementation_Plan_URL.replace('$$', IP)
    query_string = {'slaId': str(id)}
    r = requests.get(url, params=query_string)
    if r.status_code != 200:
        raise Exception
    else:
        parsed = json.loads(r.content)
        impl_plan_id = parsed['item_list'][0]['id']
        r = requests.get(url+'/'+impl_plan_id)
        if r.status_code != 200:
            raise Exception
        else:	
            return json.dumps(json.loads(r.content), indent=2, sort_keys=True)
    


class SLA:
    def __init__(self, id):
        self.id = id
        self.status = get_sla_status(id)
        self.customer = get_sla_customer(id)
        if(self.status == 'ALERTED'):
            self.hidden_status = 'ALERTED'
        elif(self.status == 'VIOLATED'):
            self.hidden_status = 'VIOLATED'
        else:
            self.hidden_status = 'OTHER'
