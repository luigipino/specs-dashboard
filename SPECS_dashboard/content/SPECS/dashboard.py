# Copyright 2015 SPECS Project - CeRICT

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#    http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# @author  Umberto Villano villano@unisannio.it
# @author  Antonio Punzo antonio.punzo@studenti.unisannio.it
# @author  Antonio Macolino antonio.macolino@gmail.com
# @author  Luigi Pino pinoluigi@hotmail.com

from django.utils.translation import ugettext_lazy as _

import horizon


class Management(horizon.PanelGroup):
    slug = "Management"
    name = _("Management")
    panels = ('Users_Management', 'SLAs_Management','Services_Management',)

class Configuration(horizon.PanelGroup):
    slug = "Configuration"
    name = _("Configuration")
    panels = ('SPECS_Options',)


class SPECS(horizon.Dashboard):
    name = _("SPECS")
    slug = "SPECS"
    panels = (Management, Configuration)  # Add your panels here.
    default_panel = 'SPECS_Options'  # Specify the slug of the dashboard's default panel.

    def nav(self, context):
        request = context['request']
        pr = request.user.token.project.get('name')
        return pr.lower() == 'specs'

horizon.register(SPECS)
