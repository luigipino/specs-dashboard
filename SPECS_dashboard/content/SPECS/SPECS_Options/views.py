# Copyright 2015 SPECS Project - CeRICT

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#    http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# @author  Umberto Villano villano@unisannio.it
# @author  Luigi Pino pinoluigi@hotmail.com

from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse_lazy
from django.core.urlresolvers import reverse

from horizon import tables
from horizon import forms
from horizon import exceptions

from tables import SPECSOptionsTable
import api

from SPECS_dashboard.content.SPECS.SPECS_Options import forms as project_forms


class IndexView(tables.DataTableView):
    template_name = 'SPECS/SPECS_Options/index.html'
    table_class = SPECSOptionsTable

    def get_data(self):
        return api.load_options()


class UpdateValueView(forms.ModalFormView):
    form_class = project_forms.UpdateValue
    modal_id = 'update_value_modal'
    modal_header = _('Update Value')
    template_name = 'SPECS/SPECS_Options/update_value.html'
    success_url = reverse_lazy('horizon:SPECS:SPECS_Options:index')
    page_title = _('Update Value')
    submit_label = _('Update')
    submit_url = 'horizon:SPECS:SPECS_Options:update_value'
    

    def _get_option(self):
        try:
            return api.get_option(self.kwargs['id'])
        except Exception:
            exceptions.handle(self.request,
                              _(' Unable to retrieve Option. '))

    def get_initial(self):
        return {'id':self.kwargs['id'], 'component':self._get_option().component, 'ip_address':self._get_option().ip_address}

    def get_context_data(self, **kwargs):
        context = super(UpdateValueView, self).get_context_data(**kwargs)
        id = self.kwargs['id']
        context['option'] = self._get_option()
        context['submit_url'] = reverse(self.submit_url, args=[id])
        return context





