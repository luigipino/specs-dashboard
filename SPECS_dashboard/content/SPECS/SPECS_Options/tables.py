# Copyright 2015 SPECS Project - CeRICT

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#    http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# @author  Umberto Villano villano@unisannio.it
# @author  Luigi Pino pinoluigi@hotmail.com

from django.utils.translation import ugettext_lazy as _

from horizon import tables

import api


class UpdateValue(tables.LinkAction):
    name = 'update_value'
    verbose_name = _('Update Value')
    icon = 'edit'
    url = 'horizon:SPECS:SPECS_Options:update_value'
    classes = ('ajax-modal',)


class UpdateRow(tables.Row):
    ajax = False

    def get_data(self, request, id):
        return api.get_option(id)


class SPECSOptionsTable(tables.DataTable):
    id = tables.Column('id', verbose_name=_('ID'), hidden= True)
    component = tables.Column('component', verbose_name=_('Component'))
    ip_address = tables.Column('ip_address', verbose_name=_('IP Address'))
    multi_select = False

    class Meta:
        name = 'SPECSOptions'
        verbose_name = _('SPECS Options')
        multi_select = False
#        row_class = UpdateRow
        row_actions = (UpdateValue,)
