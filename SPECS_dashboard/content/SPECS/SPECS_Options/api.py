# Copyright 2015 SPECS Project - CeRICT

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#    http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# @author  Umberto Villano villano@unisannio.it
# @author  Luigi Pino pinoluigi@hotmail.com

import MySQLdb
import os
import ConfigParser

RESOURCES_DIR = 'resources'
CONFIG_FILE = 'specs_options.properties'


def load_file_options():
    basepath = os.path.dirname(__file__)
    filepath = os.path.abspath(os.path.join(basepath, '..', RESOURCES_DIR, CONFIG_FILE))
    config = ConfigParser.RawConfigParser()
    config.read(filepath)
    chef_workstation_IP = config.get('EnablingPlatformSection', 'ChefWorkstationIP')
    slo_manager_IP = config.get('SPECSComponentsSection', 'SLOManagerIP')
    sla_manager_IP = config.get('SPECSComponentsSection', 'SLAManagerIP')
    service_manager_IP = config.get('SPECSComponentsSection', 'ServiceManagerIP')
    implementation_plan_IP = config.get('SPECSComponentsSection', 'ImplementationPlanIP')
    Options = []
    Options.append(SPECSOption('ChefWorkstationIP', chef_workstation_IP))
    Options.append(SPECSOption('SLOManagerIP', slo_manager_IP))
    Options.append(SPECSOption('SLAManagerIP', sla_manager_IP))
    Options.append(SPECSOption('ServiceManagerIP', service_manager_IP))
    Options.append(SPECSOption('ImpletationPlanIP', implementation_plan_IP))

    return Options

def load_options():
    db = MySQLdb.connect(host="localhost",    # your host, usually localhost
                     user="specs",         # your username
                     passwd="rkPin0",  # your password
                     db="SPECS")        # name of the data base

    cur = db.cursor()

    cur.execute("SELECT component,inet_ntoa(ipv4) FROM options")

    Options=[]

    for row in cur.fetchall():
        Options.append(SPECSOption(row[0], row[1]))

    db.close()
    return Options


def get_file_option(id):
    basepath = os.path.dirname(__file__)
    filepath = os.path.abspath(os.path.join(basepath, '..', RESOURCES_DIR, CONFIG_FILE))
    config = ConfigParser.RawConfigParser()
    config.read(filepath)
    ip_address = ''
    if 'Chef' in id:
        ip_address = config.get('EnablingPlatformSection', id)
    else:
        ip_address = config.get('SPECSComponentsSection', id)
    return SPECSOption(id, ip_address)  


def get_option(id):
    db = MySQLdb.connect(host="localhost",    # your host, usually localhost
                     user="specs",         # your username
                     passwd="rkPin0",  # your password
                     db="SPECS")        # name of the data base

    cur = db.cursor()

    sql_stmt = "SELECT component,inet_ntoa(ipv4) FROM options WHERE component=%s"
    cur.execute(sql_stmt, [id])
    row = cur.fetchone()
    db.close()
    return SPECSOption(row[0], row[1])


def update_file_value(id, ip_address):
    basepath = os.path.dirname(__file__)
    filepath = os.path.abspath(os.path.join(basepath, '..', RESOURCES_DIR, CONFIG_FILE))
    config = ConfigParser.RawConfigParser()
    config.read(filepath)
    if 'Chef' in id:
        config.set('EnablingPlatformSection', id, ip_address)
    else:
        config.set('SPECSComponentsSection', id, ip_address)
    config.write(open(filepath,'w'))
    return SPECSOption(id, ip_address)


def update_value(id, ip_address):
    db = MySQLdb.connect(host="localhost",    # your host, usually localhost
                     user="specs",         # your username
                     passwd="rkPin0",  # your password
                     db="SPECS")        # name of the data base


    cur = db.cursor()

    sql_stmt = "UPDATE options SET ipv4=inet_aton(%s) WHERE component=%s"

    try:
        cur.execute(sql_stmt, [ip_address, id])
        db.commit()
        option = SPECSOption(id, ip_address)
    except MySQLdb.IntegrityError:
        raise Exception
    finally:
        db.close()

    return option



class SPECSOption:
    def __init__(self, id, ip_address):
        self.id = id
        if 'Chef' in id:
            self.component = 'Chef Workstation IP'
        elif 'User' in id:
            self.component = 'User Manager IP'
        elif 'SLO' in id:
            self.component = 'SLO Manager IP'
        elif 'SLA' in id:
            self.component = 'SLA Manager IP'
        elif 'Service' in id:
            self.component = 'Service Manager IP'
        else:
            self.component = 'Implementation Plan IP'
        self.ip_address = ip_address
        