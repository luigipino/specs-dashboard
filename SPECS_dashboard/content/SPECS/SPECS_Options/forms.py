# Copyright 2015 SPECS Project - CeRICT

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#    http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# @author  Umberto Villano villano@unisannio.it
# @author  Luigi Pino pinoluigi@hotmail.com

from django.utils.translation import ugettext_lazy as _

from horizon import forms
from horizon import exceptions
from horizon import messages

import api


class UpdateValue(forms.SelfHandlingForm):
    id = forms.CharField(label=_('ID'), widget=forms.HiddenInput)
    component = forms.CharField(label=_('Component'), widget=forms.TextInput(attrs={'readonly': 'readonly'}))
    ip_address = forms.IPField(label=_('Insert IP address'), version=1)
    
    def handle(self, request, data):
        try:
            res = api.update_value(data['id'], data['ip_address'])
            messages.success(request, _(' IP Address updated successfully'))
            return res
        except Exception:
            exceptions.handle(request, _(' Unable to update IP address ')) 
