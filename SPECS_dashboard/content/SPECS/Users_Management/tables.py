# Copyright 2015 SPECS Project - CeRICT

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#    http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# @author  Umberto Villano villano@unisannio.it
# @author  Luigi Pino pinoluigi@hotmail.com

from django.utils.translation import ugettext_lazy as _

from horizon import tables

import api


class AddUser(tables.LinkAction):
    name = 'add_user'
    verbose_name = _('Add User')
    icon = 'plus'
    url = 'horizon:SPECS:Users_Management:add_user'
    classes = ('ajax-modal',)

class RemoveUser(tables.DeleteAction):
    name = 'remove_user'
    verbose_name = _('Remove User')
    icon = 'trash'
    requires_input = False
    data_type_singular = _('User')
    data_type_plural = _('Users')    
    classes = ('ajax-update',)

    def delete(self, request, object_id):
	       return api.remove_user(object_id)


class UsersTable(tables.DataTable):
    id = tables.Column('id', verbose_name=_('ID'), hidden= True)
    user_name = tables.Column('user_name', verbose_name=_('User Name'))
    user_password = tables.Column('user_password', verbose_name=_('User Password'))
    user_role = tables.Column('user_role', verbose_name=_('User Role'))
    multi_select = False

    class Meta:
        name = 'Users'
        verbose_name = _('Users')
        multi_select = False
        table_actions = (AddUser,)
        row_actions = (RemoveUser,)
