# Copyright 2015 SPECS Project - CeRICT

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#    http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# @author  Umberto Villano villano@unisannio.it
# @author  Luigi Pino pinoluigi@hotmail.com

from django.utils.translation import ugettext_lazy as _

from horizon import forms
from horizon import exceptions
from horizon import messages

import api


class AddUser(forms.SelfHandlingForm):
    id = forms.CharField(label=_('ID'), widget=forms.HiddenInput)
    user_name = forms.CharField(label=_('User Name'), widget=forms.TextInput)
    user_password = forms.CharField(label=_('User Password'), widget=forms.TextInput)
    user_role = forms.ChoiceField(label=_('User Role'))

    def __init__(self, *args, **kwargs):
        super(AddUser, self).__init__(*args, **kwargs)
        roles = []
        roles.append(('User', 'User'))
        roles.append(('Owner', 'Owner'))
        roles.append(('Developer', 'Developer'))
        self.fields['user_role'].choices = roles


    def handle(self, request, data):
        user_name = data.get('user_name')
        id = user_name
        user_password = data.get('user_password')
        user_role_choices = dict(self.fields['user_role'].choices)
        new_user = User(user_name, user_name, user_password, user_role)
        
        try:
	       api.add_user(new_user)
        except Exception:
            exceptions.handle(request, _(' Unable to add the new User. ')) 
        return True



