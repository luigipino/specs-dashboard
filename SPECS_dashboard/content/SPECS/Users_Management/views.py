# Copyright 2015 SPECS Project - CeRICT

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#    http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# @author  Umberto Villano villano@unisannio.it
# @author  Luigi Pino pinoluigi@hotmail.com

from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse_lazy

from tables import UsersTable
from horizon import tables
from horizon import forms

import api

from SPECS_dashboard.content.SPECS.Users_Management import forms as project_forms


class IndexView(tables.DataTableView):
    template_name = 'SPECS/Users_Management/index.html'
    table_class = UsersTable

    def get_data(self):
        return api.get_users()


class AddUserView(forms.ModalFormView):
    form_class = project_forms.AddUser
    modal_id = 'add_user_modal'
    modal_header = _('Add User')
    template_name = 'SPECS/Users_Management/add_user.html'
    success_url = reverse_lazy('horizon:SPECS:Users_Management:index')
    page_title = _('Add User')
    submit_label = _('Add')
    submit_url = reverse_lazy('horizon:SPECS:Users_Management:add_user')

