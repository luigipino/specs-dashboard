# Copyright 2015 SPECS Project - CeRICT

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#    http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# @author  Umberto Villano villano@unisannio.it
# @author  Luigi Pino pinoluigi@hotmail.com

import os
import ConfigParser

from SPECS_dashboard.content.SPECS.SPECS_Options import api as options_api

User_Manager_URL = 'http://$$:8080/user-manager/'

RESOURCES_DIR = 'resources'
CONFIG_FILE = 'specs_options.properties'


def get_file_User_Manager_IP():
    basepath = os.path.dirname(__file__)
    filepath = os.path.abspath(os.path.join(basepath, '..', RESOURCES_DIR, CONFIG_FILE))
    config = ConfigParser.RawConfigParser()
    config.read(filepath)
    IP = config.get('SPECSComponentsSection', 'USe')   
    return IP

def get_User_Manager_IP():
    option = options_api.get_option('UserManager')
    return option.ip_address

# TODO: remove stub users insertion 
def get_users():
    stub_user = User('stub-user','stub-user','user-passwd','User')
    stub_user_1 = User('stub-developer','stub-developer','dev-passwd','Developer')
    Users = []
    Users.append(stub_user)
    Users.append(stub_user_1)
    return Users

def get_user(id):
    if id == 'stub-user':
        stub_user = User('stub-user','stub-user','user-passwd','User')
    else:
        stub_user = User('stub-developer','stub-developer','dev-passwd','Developer')
    return stub_user

# TODO: implement
def add_user(user):
    pass

# TODO: implement
def remove_user(user_name):
    pass


class User:
    def __init__(self, id, user_name, user_password, user_role):
        self.id = id
        self.user_name = user_name
        self.user_password = user_password
        self.user_role = user_role