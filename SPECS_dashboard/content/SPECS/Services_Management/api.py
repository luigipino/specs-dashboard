# Copyright 2015 SPECS Project - CeRICT

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#    http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# @author  Umberto Villano villano@unisannio.it
# @author  Antonio Macolino antonio.macolino@gmail.com
# @author  Luigi Pino pinoluigi@hotmail.com


import os
import requests
from lxml import etree
import json
import urllib2
import paramiko
import ConfigParser

from SPECS_dashboard.content.SPECS.SPECS_Options import api as options_api


service_manager_URL = 'http://$$:8080/service-manager/cloud-sla/security-mechanisms/'
slo_manager_URL = 'http://$$:8080/slo-manager/sla-negotiation/sla-templates/'
RESOURCES_DIR = 'resources'
CONFIG_FILE = 'specs_options.properties'
SSH_KEY_FILE = 'cerictkpaimes.pem'


def get_file_Component_IP(component):
    basepath = os.path.dirname(__file__)
    filepath = os.path.abspath(os.path.join(basepath, '..', RESOURCES_DIR, CONFIG_FILE))
    config = ConfigParser.RawConfigParser()
    config.read(filepath)
    if('chef' in component):
        IP = config.get('EnablingPlatformSection', component)   
    else:
        IP = config.get('SPECSComponentsSection', component)   
    return IP

def get_Component_IP(component):
    option = options_api.get_option(component)
    return option.ip_address

def add_security_service(data):
    IP = get_Component_IP('SLOManager')
    url = slo_manager_URL.replace('$$', IP)
    headers = {'Content-type': 'text/xml'}
    r = requests.post(url, headers=headers, data=data)
    if r.status_code != 201:
        raise Exception
    else:
        return r.content


def get_security_service(id):
    IP = get_Component_IP('SLOManager')
    url = slo_manager_URL.replace('$$', IP)
    namespace = {'wsag': 'http://schemas.ggf.org/graap/2007/03/ws-agreement',
		'specs': 'http://www.specs-project.eu/resources/schemas/xml/SLAtemplate'}    
    r = requests.get(url + str(id))
    if r.status_code != 200:
        raise Exception
    else:
        root = etree.fromstring(r.content)
        id = root.find('wsag:Name', namespace).text
        name_root = root.find('wsag:Context', namespace)
        name = name_root.find('wsag:TemplateName', namespace).text
        mechanisms = []
        for security_capability in root.iter('{http://www.specs-project.eu/resources/schemas/xml/SLAtemplate}capability'):
            mechanisms.append(security_capability.attrib['id'])
	return SecurityService(id, name, mechanisms)



def get_security_services():
    IP = get_Component_IP('SLOManager')
    url = slo_manager_URL.replace('$$', IP)
    r = requests.get(url)
    SecurityServices = []
    root = etree.fromstring(r.content)
    for item in root :
	tag_value = item.text
        strings = tag_value.split('/')
        id = strings[len(strings)-1]
        SecurityServices.append(get_security_service(id))
    return SecurityServices


def check_capabilities(id):
    IP = get_Component_IP('ServiceManager')
    url = service_manager_URL.replace('$$', IP)
    outputs = {}
    security_service = get_security_service(id)
    for mechanism in security_service.mechanisms:
        try:
            r = requests.get(url + str(mechanism))
            outputs[str(mechanism)] = r.status_code
        except:
            outputs[str(mechanism)] = 404
    return outputs

def get_security_mechanism(id):
    IP = get_Component_IP('ServiceManager')
    url = service_manager_URL.replace('$$', IP)
    r = requests.get(url + str(id))
    if r.status_code != 200:
        raise Exception
    else:
        return json.loads(r.content)
    

def get_security_mechanisms():
    IP = get_Component_IP('ServiceManager')
    url = service_manager_URL.replace('$$', IP)
    r = requests.get(url)
    SecurityMechanisms = []
    parsed = json.loads(r.content)
    itemList = parsed['itemList']
    for item in itemList :
        strings = item.split('/')
        id = strings[len(strings)-1]
        security_mechanism_string = get_security_mechanism(id)
        SecurityMechanisms.append(SecurityMechanism(security_mechanism_string['security_mechanism_id'], security_mechanism_string['security_mechanism_name'], security_mechanism_string['security_capabilities']))
    return SecurityMechanisms


def add_security_mechanism(data):
    IP = get_Component_IP('ServiceManager')
    url = service_manager_URL.replace('$$', IP)
    headers = {'Content-type': 'text/plain'}
    r = requests.post(url, headers=headers, data=data)
    if r.status_code != 201:
        raise Exception
    else:
        return r.content

def check_artifacts(id, recipes):
    outputs = {}
    for item in recipes:
        try:
            ret = urllib2.urlopen('https://bitbucket.org/specs-team/specs-core-enforcement-repository/src/master/' + str(id) + '/recipes/' + str(item))
            outputs[str(item)] = ret.code
        except Exception:
            outputs[str(item)] = 404

    return outputs

def get_chef_recipes():
    IP = get_Component_IP('ChefWorkstation')
    basepath = os.path.dirname(__file__)
    ssh_key_path = os.path.abspath(os.path.join(basepath, '..', RESOURCES_DIR, SSH_KEY_FILE))
    key = paramiko.RSAKey.from_private_key_file(ssh_key_path)
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(hostname = IP, username = "ubuntu", pkey = key)
    command = 'knife recipe list -c /home/ubuntu/chef-repo/.chef/knife.rb'
    stdin, stdout, stderr = ssh.exec_command(command)
    output = stdout.read()
    errors = stderr.read()
    ssh.close()
    ChefRecipes = []
    line = ''
    if len(errors) > 0:
        return ChefRecipes
    for char in output:
        if not (char == '\n'):
            line = line + char
        else:    
            arr = line.split('::')
            cookbook = arr[0]
            if len(arr)>1:
                recipe = arr[1]
            else:
                recipe = '---'
            ChefRecipes.append(ChefRecipe(line, cookbook, recipe))
            line = ''
    return ChefRecipes

def update_recipes():
    IP = get_Component_IP('ChefWorkstation')
    basepath = os.path.dirname(__file__)
    ssh_key_path = os.path.abspath(os.path.join(basepath, '..', RESOURCES_DIR, SSH_KEY_FILE))
    key = paramiko.RSAKey.from_private_key_file(ssh_key_path)
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(hostname = IP, username = "ubuntu", pkey = key)
    command = 'cd /home/ubuntu/gitCookbooks && sudo ./updateCookbooks.sh'
    stdin, stdout, stderr = ssh.exec_command(command)
    output = stdout.read()
    errors = stderr.read()
    ssh.close()
    if 'successfully' in output:
        return 0
    else:
        return 1



class SLA:
    def __init__(self, id):
        self.id = id
        self.status = get_sla_status(id)
        self.customer = get_sla_customer(id)

class SecurityService:
    def __init__(self, id, name, mechanisms):
        self.id = id
        self.name = name
        self.mechanisms = mechanisms

class SecurityMechanism:
    def __init__(self, id, name, recipes):
        self.id = id
        self.name = name
        self.recipes = recipes

class ChefRecipe:
    def __init__(self, id, cookbook, recipe):
        self.id = id
        self.cookbook = cookbook
        self.recipe = recipe
