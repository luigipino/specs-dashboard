# Copyright 2015 SPECS Project - CeRICT

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#    http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# @author  Umberto Villano villano@unisannio.it
# @author  Antonio Macolino antonio.macolino@gmail.com
# @author  Luigi Pino pinoluigi@hotmail.com

from django.utils.translation import ugettext_lazy as _

from horizon import tables


class UpdateRecipes(tables.LinkAction):
    name = 'update'
    verbose_name = _('Update Recipes on Server')
    icon = 'cloud-upload'
    url = 'horizon:SPECS:Services_Management:security_mechanisms_artifacts:update'
    classes = ('ajax-modal',)



class SecurityMechanismsArtifactsTable(tables.DataTable):
    id = tables.Column('id', verbose_name=_('ID'))
    cookbook = tables.Column('cookbook', verbose_name=_('Cookbook'))
    recipe = tables.Column('recipe', verbose_name=_('Recipe'))
    multi_select = False

    class Meta:
        name = 'SecurityMechanismsArtifacts'
        verbose_name = _('SecurityMechanismsArtifacts')
        multi_select = False
        table_actions = (UpdateRecipes,)
