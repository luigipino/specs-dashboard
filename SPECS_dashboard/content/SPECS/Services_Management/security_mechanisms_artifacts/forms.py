# Copyright 2015 SPECS Project - CeRICT

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#    http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# @author  Umberto Villano villano@unisannio.it
# @author  Antonio Macolino antonio.macolino@gmail.com
# @author  Luigi Pino pinoluigi@hotmail.com

from django.utils.translation import ugettext_lazy as _

from horizon import forms
from horizon import messages

from SPECS_dashboard.content.SPECS.Services_Management import api


class UpdateRecipes(forms.SelfHandlingForm):
    slug = forms.SlugField(label=_(' This action will be executed over SSH on the Chef Workstation. Are you sure to continue? '),
				widget=forms.TextInput(attrs={'readonly': 'readonly'}),
				required=False,
				help_text=_("Download the most recent Chef recipes from repository and upload them to the Chef Server"))

    def handle(self, request, data):
        return_code = api.update_recipes()
        if return_code == 0:
            messages.success(request, _(' Recipes successfully uploaded to Chef Server '))
        else:
            messages.warning(request, _(' Failed to upload recipes to Chef Server '))
        return api.get_chef_recipes()


