# Copyright 2015 SPECS Project - CeRICT

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#    http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# @author  Umberto Villano villano@unisannio.it
# @author  Antonio Macolino antonio.macolino@gmail.com
# @author  Luigi Pino pinoluigi@hotmail.com

from django.conf.urls import url
from django.conf.urls import include

import views
from SPECS_dashboard.content.SPECS.Services_Management.security_services import urls as ss_urls
from SPECS_dashboard.content.SPECS.Services_Management.security_mechanisms_metadata import urls as smm_urls
from SPECS_dashboard.content.SPECS.Services_Management.security_mechanisms_artifacts import urls as sma_urls


urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^\?tab=services_management__security_services$', views.IndexView.as_view(), name='security_services_tab'),
    url(r'^\?tab=services_management__security_mechanisms_metadata$', views.IndexView.as_view(), name='security_mechanisms_metadata_tab'),
    url(r'^\?tab=services_management__security_mechanisms_artifacts$', views.IndexView.as_view(), name='security_mechanisms_artifacts_tab'),
    url(r'security_services/', include(ss_urls, namespace = 'security_services')),
    url(r'security_mechanisms_metadata/', include(smm_urls, namespace = 'security_mechanisms_metadata')),
    url(r'security_mechanisms_artifacts/', include(sma_urls, namespace = 'security_mechanisms_artifacts')),
]
