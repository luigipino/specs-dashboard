# Copyright 2015 SPECS Project - CeRICT

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#    http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# @author  Umberto Villano villano@unisannio.it
# @author  Antonio Macolino antonio.macolino@gmail.com
# @author  Luigi Pino pinoluigi@hotmail.com

from django.utils.translation import ugettext_lazy as _

from horizon import tabs

from SPECS_dashboard.content.SPECS.Services_Management.security_services.tables import SecurityServicesTable
from SPECS_dashboard.content.SPECS.Services_Management.security_mechanisms_metadata.tables import SecurityMechanismsMetadataTable
from SPECS_dashboard.content.SPECS.Services_Management.security_mechanisms_artifacts.tables import SecurityMechanismsArtifactsTable

import api


class SecurityServicesTab(tabs.TableTab):
    table_classes = (SecurityServicesTable,)
    name = _("Security Services")
    slug = "security_services"
    template_name = ("SPECS/Services_Management/security_services/_detail_table.html")
    preload = False

    def get_SecurityServices_data(self):
        return api.get_security_services()


class SecurityMechanismsMetadataTab(tabs.TableTab):
    table_classes = (SecurityMechanismsMetadataTable,)
    name = _("Security Mechanisms Metadata")
    slug = "security_mechanisms_metadata"
    template_name = ("SPECS/Services_Management/security_mechanisms_metadata/_detail_table.html")
    preload = False

    def get_SecurityMechanismsMetadata_data(self):
        return api.get_security_mechanisms()

class SecurityMechanismsArtifactsTab(tabs.TableTab):
    table_classes = (SecurityMechanismsArtifactsTable,)
    name = _("Security Mechanisms Artifacts")
    slug = "security_mechanisms_artifacts"
    template_name = ("SPECS/Services_Management/security_mechanisms_artifacts/_detail_table.html")
    preload = False

    def get_SecurityMechanismsArtifacts_data(self):
        return api.get_chef_recipes()


class ServicesManagementTabs(tabs.TabGroup):
    slug = 'services_management'
    tabs = (SecurityServicesTab, SecurityMechanismsMetadataTab, SecurityMechanismsArtifactsTab, )
    sticky = True
