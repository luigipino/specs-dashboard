# Copyright 2015 SPECS Project - CeRICT

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#    http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# @author  Umberto Villano villano@unisannio.it
# @author  Antonio Macolino antonio.macolino@gmail.com
# @author  Luigi Pino pinoluigi@hotmail.com

from django.utils.translation import ugettext_lazy as _

from horizon import tables
from horizon import exceptions
from horizon import messages

from SPECS_dashboard.content.SPECS.Services_Management import api


class AddService(tables.LinkAction):
    name = 'add_service'
    verbose_name = _('Add Service')
    icon = 'cloud-upload'
    url = 'horizon:SPECS:Services_Management:security_services:add_service'
    classes = ('ajax-modal',)


class CheckCapabilities(tables.Action):
    name = 'check_capabilities'
    verbose_name = _('Check Capabilities')
    requires_input = False
    icon = 'check'
    classes = ('ajax-update',)
    
    def single(self, data_table, request, object_id):
        mechanisms_found = []
        mechanisms_missing = []
        results = api.check_capabilities(object_id)
        for mechanism in results.keys():
            if (results[mechanism] == 200):
                mechanisms_found.append(mechanism)
            else:
                mechanisms_missing.append(mechanism)
        if (len(mechanisms_found) == 1):
            messages.success(request, _(' The following mechanism has been found on the SERVICE MANAGER: ' + str(mechanisms_found)))
        elif (len(mechanisms_found) > 1):
            messages.success(request, _(' The following mechanisms have been found on the SERVICE MANAGER: ' + str(mechanisms_found)))
        else:
            pass
        if (len(mechanisms_missing) == 1):
            messages.warning(request, _(' The following mechanism has NOT been found on the SERVICE MANAGER: ' + str(mechanisms_missing)))
        elif (len(mechanisms_missing) > 1):
            messages.warning(request, _(' The following mechanisms have NOT been found on the SERVICE MANAGER: ' + str(mechanisms_missing)))
        else:
            pass
        return ''



class UpdateRow(tables.Row):
    ajax = True

    def get_data(self, request, id):
        return api.get_security_service(id)


class SecurityServicesTable(tables.DataTable):
    id = tables.Column('id', verbose_name=_('ID'))
    name = tables.Column('name', verbose_name=_('Name'))
    multi_select = False

    class Meta:
        name = 'SecurityServices'
        verbose_name = _('Security Services')
        multi_select = False
        row_class = UpdateRow
        table_actions = (AddService,)
        row_actions = (CheckCapabilities,)
