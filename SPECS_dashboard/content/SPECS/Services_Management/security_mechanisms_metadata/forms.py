# Copyright 2015 SPECS Project - CeRICT

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#    http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# @author  Umberto Villano villano@unisannio.it
# @author  Antonio Macolino antonio.macolino@gmail.com
# @author  Luigi Pino pinoluigi@hotmail.com

from django.utils.translation import ugettext_lazy as _

from horizon import forms
from horizon import exceptions

from SPECS_dashboard.content.SPECS.Services_Management import api


class AddSecurityMechanism(forms.SelfHandlingForm):
    FileField = forms.FileField
    mechanism_file = FileField(label=_("Mechanism File"),
                           help_text=_("A local .json file to upload, representing the Security Mechanism."),
                           required=True)
    def handle(self, request, data): 
        try:
            file_content = 'errore'
            uploaded_file = data['mechanism_file']
            uploaded_file.open()
            strings = uploaded_file.file.readlines()
            file_content = '\n'.join(strings)
            return api.add_security_mechanism(file_content)
        except Exception:
            exceptions.handle(request, file_content)
	    return False

