# Copyright 2015 SPECS Project - CeRICT

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#    http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# @author  Umberto Villano villano@unisannio.it
# @author  Antonio Macolino antonio.macolino@gmail.com
# @author  Luigi Pino pinoluigi@hotmail.com

from django.utils.translation import ugettext_lazy as _

from horizon import tables
from horizon import messages

from SPECS_dashboard.content.SPECS.Services_Management import api


class AddSecurityMechanism(tables.LinkAction):
    name = 'add_security_mechanism'
    verbose_name = _('Add Security Mechanism (Metadata)')
    icon = 'cloud-upload'
    url = 'horizon:SPECS:Services_Management:security_mechanisms_metadata:add_security_mechanism'
    classes = ('ajax-modal',)


class CheckArtifacts(tables.Action):
    name = 'check_artifacts'
    verbose_name = _('Check Artifacts')
#    method = 'POST'
    requires_input = False
    icon = 'check'
    classes = ('ajax-update',)    

    def single(self, data_table, request, object_id):
        recipes_to_check = []
        mechanism_json = api.get_security_mechanism(object_id)
        for recipe in mechanism_json['chef_recipes']:
            recipes_to_check.append(str(recipe['name'] + '.rb'))
        if (len(recipes_to_check) == 0):
            messages.info(request, _(' NO recipes to check'))
            return ''
        results = api.check_artifacts(object_id, recipes_to_check)
        recipes_found = []
        recipes_missing = []
        for recipe in recipes_to_check:
            if (results[recipe] == 200):
                recipes_found.append(recipe)
            else:
                recipes_missing.append(recipe)
        if (len(recipes_found) == 1):
            messages.success(request, _(' The following recipe has been found on repo: ' + str(recipes_found)))
        elif (len(recipes_found) > 1):
            messages.success(request, _(' The following recipes have been found on repo: ' + str(recipes_found)))
        else:
            pass
        if (len(recipes_missing) == 1):
            messages.warning(request, _(' The following recipe has NOT been found on repo: ' + str(recipes_missing)))
        elif (len(recipes_missing) > 1):
            messages.warning(request, _(' The following recipes have NOT been found on repo: ' + str(recipes_missing)))
        else:
            pass
        return ''


class UpdateRow(tables.Row):
    ajax = True
    def get_data(self, request, id):
        return api.get_security_mechanism(id)


class SecurityMechanismsMetadataTable(tables.DataTable):
    id = tables.Column('id', verbose_name=_('ID'))
    name = tables.Column('name', verbose_name=_('Name'))
    multi_select = False

    class Meta:
        name = 'SecurityMechanismsMetadata'
        verbose_name = _('SecurityMechanismsMetadata')
        multi_select = False
        row_class = UpdateRow
        table_actions = (AddSecurityMechanism,)
        row_actions = (CheckArtifacts,)
