import pbr.version


__version__ = pbr.version.VersionInfo(
    'SPECS-dashboard').version_string()
