# The name of the dashboard to be added to HORIZON['dashboards']. Required.
DASHBOARD = 'SPECS'

# If set to True, this dashboard will not be added to the settings.
DISABLED = False

# A list of applications to be added to INSTALLED_APPS.
ADD_INSTALLED_APPS = ['SPECS_dashboard.content.SPECS',]

# Automatically discover static resources in installed apps
AUTO_DISCOVER_STATIC_FILES = True

